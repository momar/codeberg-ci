# Codeberg CI

## What is it?
Codeberg CI is a Continuous Integration platform for Gitea, that is built for simplicity & flexibility, and offers the following features:
- tasks based on https://taskfile.dev, because it's simple, flexible & works cross-platform
- environment-based secrets (not accessible from forks)
- step conditions (based on e. g. branch or commit), using Taskfile's preconditions
- multiple operating system images based on libvirt (Codeberg will only provide `alpine` and `debian`)
- resource limits for CPU, RAM, disk space (also for logs) and build time, adjustable per user/organization or repository
- caching for APK/DEB repositories & Docker images
- secure public and private workers
- full Gitea integration (as far as possible with the Gitea API)

## Example `Taskfile.yml`

```yaml
version: '3'

tasks:
  ci:
    cmds:
      - apk add --no-cache go
      - task: build
      - task: release
    vars:
      WORKER_IMAGES: alpine

  build:
    cmds:
      - go build -v -ldflags '-s -w' -o ./example main.go

  release:
    cmds:
      - tea release create --tag "$GIT_TAG" --title "$GIT_TAG" --asset ./example
    conditions:
      - test -n "$GIT_TAG" # must be non-empty
```

## Current Progress
- [ ] Run a method in the manager whenever a Gitea repo updates (#1)
- [ ] Implement simple secret store & configuration (worker key that can be overwritten by a secret, resource limits) (#2)
- [ ] Delegate the build to the correct worker (with multiple workers being allowed to share a secret) (#3)
- [ ] Run the taskfile's `ci` task in a worker (#4)
- [ ] Put the progress/results back into the Gitea API through the manager (#5)
- [ ] Provide neccessary environment variables to for Taskfile's preconditions (#6)
- [ ] Implement log storage & viewer (#7)
- [ ] Make secrets configurable by repo owners (write-only) (#8)
- [ ] Use libvirt for virtualization (#9)
- [ ] Select worker image based on `tasks.ci.vars.WORKER_IMAGES` (with the possibility to run multiple simultaneous builds on different images!) (#10)
- [ ] Populate the `WORKER_IMAGE` variable in the VM (#11)
- [ ] Enforce resource limits in libvirt (#12)
- [ ] Enforce maximum per-build time and daily/weekly/monthly build seconds (#13)
- [ ] Cache APK/DEB repositories & Docker images (#14)
- [ ] Smartly manage available disk space, log retention, and so on (#15)
- [ ] Choose sane defaults for resource limits (#16)
- [ ] Add TOML support to Taskfile (#17)
- [ ] Make sure that conditions are working correctly without causing the pipeline to fail (like preconditions) (#18)
